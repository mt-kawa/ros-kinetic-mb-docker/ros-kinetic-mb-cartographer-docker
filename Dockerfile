FROM ros:kinetic-ros-core
MAINTAINER Kai Waelti <Kai.Waelti@dfki.de>

RUN apt-get update && apt-get install -y python-wstool python-rosdep ninja-build build-essential

RUN mkdir -p /root/catkin_ws && \
    cd /root/catkin_ws && \
    wstool init src && \
    wstool merge -t src https://raw.githubusercontent.com/googlecartographer/cartographer_ros/master/cartographer_ros.rosinstall && \
    wstool update -t src

WORKDIR /root/catkin_ws

RUN set -o errexit && set -o verbose && \
    VERSION="v3.4.1" && \
    git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    git checkout tags/${VERSION} && \
    mkdir build && cd build && \
    cmake -G Ninja \
        -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
        -DCMAKE_BUILD_TYPE=Release \
        -Dprotobuf_BUILD_TESTS=OFF \
        ../cmake && \
    ninja && \
    ninja install

RUN rosdep install --from-paths src --ignore-src --rosdistro=kinetic -y

SHELL ["/bin/bash", "-c"]	# Change to bash shell for ros stuff

RUN source /opt/ros/kinetic/setup.bash && \
    catkin_make_isolated --install --use-ninja -j2

COPY launch-files /launch-files
COPY run-shells /run-shells

WORKDIR /root

COPY urdf catkin_ws/install_isolated/share/cartographer_ros/urdf/
COPY config catkin_ws/install_isolated/share/cartographer_ros/configuration_files/
COPY entrypoint.sh .

ENTRYPOINT ["/root/entrypoint.sh"]

ENV ROS_MASTER_URI "http://ros-master:11311"

CMD ["bash"]

