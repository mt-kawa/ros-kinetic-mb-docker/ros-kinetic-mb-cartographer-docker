#!/bin/bash

source /ros_entrypoint.sh

# Set catkin workspace
echo "Source ROS and cartographer mb ws..."
source /opt/ros/kinetic/setup.bash
source /root/catkin_ws/install_isolated/setup.bash

