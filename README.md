# Cartographer ROS Compiled for ros-kinetic-mb

Run this container to run Google's Cartographer SLAM on the Mobility Base and generate a map from ros-kinetic-mb containers.

## Getting image

To get the image from RepoHub

```
docker pull repohub.enterpriselab.ch:5002/kawa/ros-kinetic-mb-cartographer:latest
```

Execute the following commands to build the container yourself

```
git clone https://gitlab.enterpriselab.ch/mt-kawa/ros-kinetic-mb-docker/ros-kinetic-mb-cartographer-docker.git
cd ros-kinetic-mb-cartographer-docker
docker build -t ros-kinetic-mb-cartographer:latest .
```

## Run SLAM

To run SLAM run the following commands

```
docker run -it ros-kinetic-mb-cartographer roslaunch /run-shells/cartographer.launch
```

